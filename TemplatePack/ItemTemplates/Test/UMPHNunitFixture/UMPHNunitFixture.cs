﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace $rootnamespace$
{
    [TestFixture]
    public class $safeitemname$ : IntegrationTest
    {

        [SetUp]
        public void Setup()
        {

        }

        #region Test Calls

        [Test]
        public void Call()
        {
            PerformTest("Test");
        }

        #endregion

        #region Test Methods

        private void Test()
        {
            VerifyExecutingInTransaction();

            throw new NotImplementedException();
        }

        #endregion

    }
}