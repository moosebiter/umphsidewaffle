﻿Purpose
This is a SideWaffle installation that has several custom templates used by the United Methodist Publishing House web development team.

Table Of Contents:
1. Installation and updates
2. Usage

Installation and updates
1. Remove any existing SideWaffle Template packs through the Visual Studio:
	Tools > Extensions and Updates... > SideWaffle Template Pack > Uninstall
2. run the "UMPHTemplatePack.vsix" provided

Usage
1. Go to the directory where you would like to create a test.
2. Right Click the directory and add > New Item...
3. Go to Visual C# Items > Tests in the left nav or search for "UMPH NUnit Fixture"
4. Rename the file to your test class name and click "Add"
5. Write tests in the generated template